from django import forms

from .models import FileUpload


class FileUploadForm(forms.ModelForm):

    class Meta:
        model = FileUpload
        fields = ('file_path',)


class FileFieldForm(forms.Form):
    file_field = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
from django.shortcuts import render
from django.views.generic import FormView
from .forms import FileFieldForm
from django.core.urlresolvers import reverse_lazy
from .models import FileUpload

# Create your views here.
def my_files_view(request):

    query_files = FileUpload.objects.filter(user=request.user).values_list('file_path')

    file_dict = {}
    for f in [path[0] for path in query_files]:
        get_all_filepaths(file_dict, f.split("/"))

    return render(request, 'myfiles.html', context={'set_of_files': file_dict})


def get_all_filepaths(file_dict, array):
    if len(array) == 0:
        return
    elif len(array) == 1:
        file_dict.append(array[0])
    else:
        get_all_filepaths(file_dict.setdefault(array[0], [] if len(array) == 2 else {}), array[1:])

class FileFieldView(FormView):

    form_class = FileFieldForm
    template_name = 'upload.html'
    success_url = reverse_lazy('model_upload')

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        files = request.FILES.getlist('file_field')
        if form.is_valid():
            for f in files:
                print(str(f))
                file_upload_obj = FileUpload(user=request.user , file_path = f, file_name=str(f))
                file_upload_obj.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


from django.conf.urls import url
from file_upload.views import FileFieldView, my_files_view

urlpatterns = [
    url(r'^$', FileFieldView.as_view(), name='model_upload'),
    url(r'^myfiles/', my_files_view ,name ='myfiles')
]

from django.conf.urls import url
from .views import home, register

urlpatterns = [
    url(r'^$', home),
    url(r'^register/', register),
]
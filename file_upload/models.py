from __future__ import unicode_literals

from django.db import models
from media_upload.settings import MEDIA_ROOT
from datetime import date
import os
from django.contrib.auth.models import User


def user_image_folder(instance = None, filename = None):
    today = date.today()
    today_path = today.strftime("%Y/%B/%d")

    file_ext = str(filename).split('.')
    if file_ext[1] in ('jpg', 'png'):
        return today_path + '/images/content/' + filename
    if file_ext[1] in ('mp4', 'mov', 'avi'):
        return today_path + '/videos/content/' + filename


class FileUpload(models.Model):

    class Meta:
        db_table = 'FileUpload'

    today = date.today()
    today_path = today.strftime("%Y/%m/%d")
    os.path.join(MEDIA_ROOT, today_path)

    user = models.ForeignKey(User, blank=True)
    file_name = models.CharField(max_length=255, blank=True)
    file_path = models.FileField(upload_to=user_image_folder)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):

        return self.user.username + " -- " + str(self.file_path.url) + " >> " + str(self.file_name) + "  " + str(self.uploaded_at)

